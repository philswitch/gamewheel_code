#include "raylib-cpp.hpp"
#include <iostream>
#include <array>

#undef DEG2RAD
#define DEG2RAD(x) x * PI / 180.0f

struct Player
{
	raylib::Vector2 position;
	raylib::Vector2 velocity;
	float rotation;
	raylib::Texture2D texture;
	bool dead;
	bool shoot;
	double nextShoot;
};

struct Asteroid
{
	bool active;
	raylib::Vector2 position;
	raylib::Vector2 velocity;
	float spriteRotation;
	raylib::Texture2D* texture;
	float lifetime;
};

struct Bullet
{
	bool active;
	raylib::Vector2 position;
	raylib::Vector2 velocity;
	float rotation;
	raylib::Texture2D* texture;
	float lifetime;
};

raylib::Texture2D smallAsteroidTex;
raylib::Texture2D bigAsteroidTex;
raylib::Texture2D bulletTex;
raylib::Text gameOverText;

Player player;
std::array<Asteroid, 255> asteroids;
std::array<Bullet, 255> bullets;
#define PLAYER_SPEED 200
#define PLAYER_ROTATION_SPEED 500
#define SHOOT_COOLDOWN 0.4;
#define BULLET_SPEED 600.0f


void Shoot(raylib::Vector2 position, raylib::Vector2 direction)
{
	for (unsigned int i = 0; i < bullets.size(); ++i) 
	{
		if (bullets[i].active) continue;
		bullets[i].position = position;
		bullets[i].velocity = direction * BULLET_SPEED;
		bullets[i].rotation = player.rotation;
		bullets[i].lifetime = 1;
		bullets[i].texture = &bulletTex;

		bullets[i].active = true;
		break;
	}
}

void ProcessPlayer()
{
	float rotate = (float)IsKeyDown(KEY_RIGHT) - (float)IsKeyDown(KEY_LEFT);
	player.rotation += rotate * PLAYER_ROTATION_SPEED * GetFrameTime();

	raylib::Vector2 movement{0.0f, (float)IsKeyDown(KEY_UP) - (float)IsKeyDown(KEY_DOWN)};
	movement = movement.Rotate(DEG2RAD(player.rotation));
	movement = movement.Normalize();
	player.velocity = movement * PLAYER_SPEED * GetFrameTime();

	player.shoot = IsKeyDown(KEY_SPACE);
}

#define TICKRATE 3
float ticksPassed;
float randUpperTicks;

const raylib::Vector2 spawn_point{850.0f, 0.0f};
#define VEL_VARIATION 14

void Tick()
{
	ticksPassed += GetFrameTime();
	if(ticksPassed < randUpperTicks) return;
	ticksPassed = 0;
	
	randUpperTicks = GetRandomValue(TICKRATE, TICKRATE + 2);

	for (unsigned int i = 0; i < asteroids.size(); ++i) 
	{
		if(asteroids[i].active) continue;

		float num1 = GetRandomValue(0, 359);
		float num2 = GetRandomValue(0, 59);
		num2 /= 100;
		num1 += num2;

		asteroids[i].position = spawn_point.Rotate(DEG2RAD(num1));
		asteroids[i].velocity = (raylib::Vector2{300.0f, 400.0f} - asteroids[i].position) * 0.1;
		asteroids[i].velocity = asteroids[i].velocity.Rotate(DEG2RAD(GetRandomValue(-VEL_VARIATION, VEL_VARIATION)));
		asteroids[i].texture = &bigAsteroidTex;
		asteroids[i].lifetime = (float)GetRandomValue(10, 50);
		asteroids[i].active = true;
		break;
	}
}

void Render(raylib::Window& window)
{
	window.ClearBackground(BLACK);

	if(player.dead)
	{
		gameOverText.Draw(150.0f, 350.0f);
		return;
	}

	//Drawing the player
	player.position -= player.velocity;
	raylib::Rectangle plSrcRect{raylib::Vector2{0.0f, 0.0f}, player.texture.GetSize()};
	raylib::Rectangle plDstRect{player.position, player.texture.GetSize()};
	raylib::Vector2 plOrigin{ player.texture.GetWidth() / 2.0f, player.texture.GetHeight() / 2.0f};
	player.texture.Draw(plSrcRect, plDstRect, plOrigin, player.rotation);

	if (player.shoot && GetTime() >= player.nextShoot) 
	{
		Shoot(plDstRect.GetPosition(), raylib::Vector2{0.0f, 1.0f}.Rotate(DEG2RAD(player.rotation)));
		player.shoot = false;
		player.nextShoot = GetTime() + SHOOT_COOLDOWN;
 	}

	for (unsigned int i = 0; i < bullets.size(); ++i) 
	{
		if (!bullets[i].active) continue;
		
		bullets[i].position -= bullets[i].velocity * GetFrameTime();

		raylib::Rectangle blSrcRect{raylib::Vector2{0.0f, 0.0f}, bullets[i].texture->GetSize()};
		raylib::Rectangle blDstRect{bullets[i].position, bullets[i].texture->GetSize()};
		raylib::Vector2 blOrigin{bullets[i].texture->GetWidth() / 2.0f, bullets[i].texture->GetHeight() / 2.0f};
		bullets[i].texture->Draw(blSrcRect, blDstRect, blOrigin, bullets[i].rotation);

		for(unsigned int j = 0; j < asteroids.size(); ++j)
		{
			if (!asteroids[j].active) continue;

			raylib::Vector2 asOrigin{asteroids[j].texture->GetWidth() / 2.0f, asteroids[j].texture->GetHeight() / 2.0f};
			if (bullets[i].position.Distance(asteroids[j].position) < 100 && blDstRect.CheckCollision(asteroids[j].position + asOrigin, 4.5)) 
			{
				bullets[i].active = false;
				asteroids[j].active = false;
  			}
		}

		bullets[i].lifetime -= GetFrameTime();
		if(bullets[i].lifetime <= 0)
		{
			bullets[i].active = false;
		}
	}

	//Draw the asteroids
	for (unsigned int i = 0; i < asteroids.size(); ++i) 
	{
		if (!asteroids[i].active) continue;

		asteroids[i].position += asteroids[i].velocity * GetFrameTime();

		raylib::Rectangle asSrcRect{raylib::Vector2{0.0f, 0.0f}, asteroids[i].texture->GetSize()};
		raylib::Rectangle asDstRect{asteroids[i].position, asteroids[i].texture->GetSize()};
		raylib::Vector2 asOrigin{asteroids[i].texture->GetWidth() / 2.0f, asteroids[i].texture->GetHeight() / 2.0f};
		asteroids[i].texture->Draw(asSrcRect, asDstRect, asOrigin, asteroids[i].spriteRotation);

		if (asteroids[i].position.Distance(player.position) < 200 && plDstRect.CheckCollision(asteroids[i].position + asOrigin, 4.5f))
		{
			player.dead = true;
  		}
		
		asteroids[i].lifetime -= GetFrameTime();
		if (asteroids[i].lifetime <= 0) 
		{
			asteroids[i].active = false;
  		}
 	}
}

int main(int argc, char *argv[])
{
	ticksPassed = 0;

	int screenW = 600;
	int screenH = 800;

	player.position = raylib::Vector2{300.0f, 400.0f};
	player.rotation = 0.0f;
	player.nextShoot = 0;
	
	raylib::Window window(screenW, screenH, "Asteroids_CPP");
	SetTargetFPS(60);

	player.texture = raylib::Texture2D("./assets/ship_E.png");
	smallAsteroidTex = raylib::Texture2D("./assets/meteor_small.png");
	bigAsteroidTex = raylib::Texture2D("./assets/meteor_large.png");
	bulletTex = raylib::Texture2D("./assets/ship_B.png");

	gameOverText = raylib::Text("      GAME OVER\nInsert another coin", 30, RAYWHITE, GetFontDefault(), 2);

	while (!window.ShouldClose()) 
	{
		BeginDrawing();
		ProcessPlayer();
		Tick();
		Render(window);
		EndDrawing();
	}

	return 0;
}
